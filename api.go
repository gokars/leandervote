package main

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gokars/leandervote/domain"
	"github.com/gokars/leandervote/handlers"
	"github.com/gokars/leandervote/services"
	log "github.com/sirupsen/logrus"
)

const (
	get  = "GET"
	post = "POST"
	opt  = "OPTIONS"
)

// API holds the api handlers
type API struct {
	encryptionKey []byte
	ACL           services.ACLService

	Tokens  *handlers.Tokens
	Users   *handlers.Users
	Votings *handlers.Votings
}

// NewAPI will be called with cert and key later
func NewAPI() *API {
	// FIXME: Generated key
	encryptionKey := []byte("secret")

	aclService := services.NewACLService()
	tokenService := services.NewTokenService(encryptionKey)
	userService := services.NewUserService()
	votingService := services.NewVotingService()

	return &API{
		encryptionKey: encryptionKey,
		ACL:           aclService,
		Tokens:        handlers.NewTokens(tokenService),
		Users:         handlers.NewUsers(userService, tokenService),
		Votings:       handlers.NewVotings(votingService, userService, tokenService),
	}
}

// Middleware section

// Authenticate provides authentication middleware for handlers
func (a *API) Authenticate() gin.HandlerFunc {
	return func(c *gin.Context) {
		var tokenStr string
		// Get token from the Authorization header
		// format: Authorization: Bearer <token>
		tokens, ok := c.Request.Header["Authorization"]
		if ok && len(tokens) >= 1 {
			tokenStr = tokens[0]
			tokenStr = strings.TrimPrefix(tokenStr, "Bearer ")
		}

		if tokenStr == "" {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		// FIXME: Create new package with usage of Intel SGX to make key safe
		token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
			// FIXME: Algorithm type from config
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, domain.ErrInvalidAlogrithm
			}
			return a.encryptionKey, nil
		})
		if err != nil {
			log.Debug("[Middleware][Authentication] Invalid Token: ", tokenStr)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			c.Set("user", claims["user"])   // email string
			c.Set("admin", claims["admin"]) // admin bool
			c.Next()
			return
		}
		// FIXME: What status here when problem is on server side (problem with parsing token here)?
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
}

// Authorize provides authorization middleware for handlers
func (a *API) Authorize(permissions ...services.Permission) gin.HandlerFunc {
	return func(c *gin.Context) {
		// FIXME: Consider changing api to use this middleware
	}
}

// SecureHeaders adds secure headers to the responses
func (a *API) SecureHeaders() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Content-type", "application/json")
		//c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Origin, Accept, token")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		c.Next()
	}
}

// SecureOptions set response's header for OPTIONS request
func (a *API) SecureOptions(m ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		// TODO: Interesting, memory is not cleared for m, every request is adding "OPTIONS" again
		// m = append(m, "OPTIONS")
		methods := strings.Join(m, ",")
		c.Writer.Header().Set("Access-Control-Allow-Methods", methods)
		c.Writer.Header().Set("Vary", "Origin")
		c.Writer.Header().Set("Vary", "Access-Control-Request-Method")
		c.Writer.Header().Set("Vary", "Access-Control-Request-Headers")
		c.Status(http.StatusOK)
	}
}
