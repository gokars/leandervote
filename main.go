package main

import (
	"os"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

var router *gin.Engine

func init() {
	// For log analysis ..
	// log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	// FIXME: Get level from config
	log.SetLevel(log.DebugLevel)
}

func main() {
	// FIXME: Production mode
	// Set Gin to production mode
	// gin.SetMode(gin.ReleaseMode)
	router = gin.Default()

	router.GET("/")

	// FIXME: Call api with certs for HTTPS
	api := NewAPI()

	v1 := router.Group("/v1")
	{
		v1.Use(api.SecureHeaders())
		v1.OPTIONS("/login", api.SecureOptions(post, opt))
		v1.OPTIONS("/logout", api.SecureOptions(get, opt))
		v1.OPTIONS("/register", api.SecureOptions(post, opt))
		v1.OPTIONS("/voting", api.SecureOptions(get, post, opt))
		v1.OPTIONS("/voting/:id", api.SecureOptions(get, opt))
		v1.OPTIONS("/voting/:id/*action", api.SecureOptions(get, opt))
		v1.POST("/login", api.Users.Login)
		v1.GET("/logout", api.Users.Logout)
		v1.POST("/register", api.Votings.Register)
		v1.Use(api.Authenticate(), api.Authorize())
		v1.GET("/voting", api.Votings.GetVotings)
		v1.POST("/voting", api.Votings.Add)
		v1.GET("/voting/:id", api.Votings.Single)
		v1.GET("/voting/:id/*action", api.Votings.Action)
	}

	router.Run(":9000")
}
