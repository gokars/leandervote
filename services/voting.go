package services

import (
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"

	"github.com/gokars/leandervote/domain"
	"github.com/gokars/leandervote/store"
	"github.com/gokars/leandervote/utils"
	"gopkg.in/mgo.v2/bson"
)

// Votings ..
type Votings interface {
	Get(ids []string, admin bool) ([]domain.VotingShort, error)
	Add(domain.Voting) error
	Single(string) (domain.Voting, error)
	Invite(id string, voters []domain.Voter) error
	IsTokenUsed(id, email string) (bool, string, error)
	SetUserRegistered(id, email string) error
	GetVoterStatus(id, email string) (domain.VoterInfo, error)
	NewClaims(id, email string, hours int) VotingClaims
}

type votingService struct{}

// NewVotingService ..
func NewVotingService() Votings {
	return &votingService{}
}

// Get ..
func (v *votingService) Get(ids []string, admin bool) ([]domain.VotingShort, error) {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("votings")
	votingStore := store.NewVotingStore(col)
	results, err := votingStore.Get(ids, admin)
	if err != nil {
		return nil, err
	}

	return results, nil
}

// Single ..
func (v *votingService) Single(id string) (domain.Voting, error) {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("votings")
	votingStore := store.NewVotingStore(col)
	results, err := votingStore.Single(id)
	if err != nil {
		return domain.Voting{}, err
	}

	return results, nil
}

// Add ..
func (v *votingService) Add(voting domain.Voting) error {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("votings")
	votingStore := store.NewVotingStore(col)
	voting.ID = bson.NewObjectId()
	// maybe convert time also
	err := votingStore.Create(voting)
	if err != nil {
		log.Println("Voting Add error")
		return err
	}
	return nil
}

func (v *votingService) NewClaims(id, email string, hours int) VotingClaims {
	claims := VotingClaims{
		Email:  email,
		Voting: id,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(hours)).Unix(),
			Issuer:    "test",
		},
	}
	return claims
}

// Invite ..
func (v *votingService) Invite(id string, voters []domain.Voter) error {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("votings")
	votingStore := store.NewVotingStore(col)

	for index := range voters {
		// TODO: MAILMAN - send email with token to voter
		voters[index].TokenSent = true
	}

	log.Info("Emails with invitation sent to '%d' voters", len(voters))

	err := votingStore.UpdateVoters(id, voters)
	if err != nil {
		return err
	}
	return nil
}

// IsTokenUsed
func (v *votingService) IsTokenUsed(id, email string) (bool, string, error) {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("votings")
	votingStore := store.NewVotingStore(col)

	voter, err := votingStore.GetVoter(id, email)
	if err != nil {
		return false, "", err
	}
	if voter.Registered {
		return true, "", nil
	}
	return false, voter.TokenHash, nil
}

// SetUserRegistered ..
func (v *votingService) SetUserRegistered(id, email string) error {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("votings")
	votingStore := store.NewVotingStore(col)

	voter, err := votingStore.GetVoter(id, email)
	voter.Registered = true

	log.Info("User '%s' registered", email)

	err = votingStore.UpdateVoter(id, voter)
	if err != nil {
		return err
	}
	return nil
}

// GetVoterStatus ..
func (v *votingService) GetVoterStatus(id, email string) (domain.VoterInfo, error) {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("votings")
	votingStore := store.NewVotingStore(col)
	voter, err := votingStore.GetVoter(id, email)

	if err != nil {
		return domain.VoterInfo{}, nil
	}

	info := domain.VoterInfo{
		Registered: voter.Registered,
		Signed:     voter.Signed,
		Authorized: voter.Authorized,
	}

	return info, nil
}
