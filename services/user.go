package services

import (
	"github.com/gokars/leandervote/domain"
	"github.com/gokars/leandervote/store"
	"github.com/gokars/leandervote/utils"
	log "github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
)

// Users provides interfaces for managing users
type Users interface {
	Create(email, pass, voting string) error
	Read(string) (domain.UserDetails, error)
	Update(domain.User) error
	Delete(int) error
	Authorize(string, string) (bool, error)
	GetToken(domain.User) (string, error)
	IsExists(string) (bool, error)
	AddVoting(email string, voting string) error
	GetVotings(email string) []string
	AuthorizeWithRole(email, pass, role string) (bool, bool, error)
}

type userService struct{}

// NewUserService creates a new UserService
func NewUserService() Users {
	return &userService{}
}

// IsExists ..
func (us *userService) IsExists(email string) (bool, error) {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("users")
	userStore := store.NewUserStore(col)

	n, err := userStore.C.Find(bson.M{"email": email}).Count()
	if err != nil {
		return false, err
	}
	log.Debug("Records fetched for email '%s': '%d'", email, n)
	if n == 0 {
		return false, nil
	}
	return true, nil
}

// Create ..
func (us *userService) Create(email, pass, voting string) error {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("users")
	userStore := store.NewUserStore(col)
	hash, err := utils.HashPass(pass)
	if err != nil {
		return domain.ErrHashPass
	}
	ud := domain.UserDetails{
		ID:      bson.NewObjectId(),
		Email:   email,
		Hash:    hash,
		Roles:   []string{"user"},
		Votings: []string{voting},
	}
	err = userStore.Create(ud)
	if err != nil {
		return domain.ErrCreateDatabaseRecord
	}
	return nil
}

// Read ..
func (us *userService) Read(email string) (domain.UserDetails, error) {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("users")
	userStore := store.NewUserStore(col)

	var ud domain.UserDetails
	err := userStore.C.Find(bson.M{"email": email}).One(&ud)
	if err != nil {
		return domain.UserDetails{}, domain.ErrQueryDatabase
	}
	return ud, nil
}

// Update ..
func (us *userService) Update(u domain.User) error {
	return nil
}

// Delete ..
func (us *userService) Delete(id int) error {

	return nil
}

// Authorize ..
func (us *userService) Authorize(email string, pass string) (bool, error) {
	user, err := us.Read(email)
	if err != nil {
		return false, err
	}
	_, err = utils.CompareHash(user.Hash, pass)
	if err != nil {
		return false, err
	}
	return true, nil
}

// AuthorizeWithRole ..
func (us *userService) AuthorizeWithRole(email, pass, role string) (bool, bool, error) {
	user, err := us.Read(email)
	if err != nil {
		return false, false, err
	}
	_, err = utils.CompareHash(user.Hash, pass)
	if err != nil {
		return false, false, err
	}
	if user.HasRole(role) {
		return true, true, nil
	}
	return true, false, nil
}

// GetToken ..
func (us *userService) GetToken(u domain.User) (string, error) {
	return "", nil
}

// GetVotings ..
func (us *userService) GetVotings(email string) []string {
	user, err := us.Read(email)
	if err != nil {
		return nil
	}
	return user.Votings
}

// AddVoting ..
func (us *userService) AddVoting(email string, voting string) error {
	ds := utils.NewDataStore()
	defer ds.Close()
	col := ds.Collection("users")
	userStore := store.NewUserStore(col)

	err := userStore.AddVoting(email, voting)
	if err != nil {
		return err
	}
	return nil
}
