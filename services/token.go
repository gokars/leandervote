package services

import (
	"errors"
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// Set our secret.
// TODO: Use generated key from README
var mySigningKey = []byte("secret")

// Token defines a token for our application
type Token string

// TokenService provides a token
type TokenService interface {
	ParseToken(t string) (*jwt.Token, error)
	GetAuthToken(c CustomClaims) (string, error)
	GetVotingToken(v VotingClaims) (string, error)
	ParseRegisterToken(string) (*jwt.Token, VotingClaims, error)
	GetAuthClaims(admin bool, email string, hours int) CustomClaims
}

type tokenService struct {
	Key []byte
}

// CustomClaims type holds the token claims
type CustomClaims struct {
	Admin bool   `json:"admin"`
	User  string `json:"user"`
	jwt.StandardClaims
}

// VotingClaims ..
type VotingClaims struct {
	Email  string `json:"email"`
	Voting string `json:"voting"`
	jwt.StandardClaims
}

// NewTokenService creates a new UserService
func NewTokenService(k []byte) TokenService {
	return &tokenService{k}
}

// GetAuthClaims
func (s *tokenService) GetAuthClaims(admin bool, email string, hours int) CustomClaims {
	claims := CustomClaims{
		Admin: admin,
		User:  email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * time.Duration(hours)).Unix(),
			Issuer:    "test",
		},
	}
	return claims
}

// Get retrieves a token for a user
func (s *tokenService) GetAuthToken(claims CustomClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign token with key
	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", errors.New("Failed to sign token")
	}

	return tokenString, nil
}

// GetVotingToken ..
func (s *tokenService) GetVotingToken(claims VotingClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign token with key
	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", errors.New("Failed to sign token")
	}

	return tokenString, nil
}

func (s *tokenService) ParseToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			msg := fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			return nil, msg
		}
		return s.Key, nil
	})
	return token, err
}

func (s *tokenService) ParseRegisterToken(tokenString string) (*jwt.Token, VotingClaims, error) {
	var claims VotingClaims
	token, err := jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			msg := fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			return nil, msg
		}
		return s.Key, nil
	})
	return token, claims, err
}
