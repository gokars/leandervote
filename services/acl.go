package services

import (
	"errors"

	"github.com/gokars/leandervote/domain"
)

// ACLService regulates access control.
type ACLService interface {
	CheckPermission(*domain.UserDetails, Permission) error
}

// NewACLService creates an access control service
func NewACLService() ACLService {
	return &aclService{}
}

type aclService struct{}

// Permission is a type of permission
type Permission string

//AdministratorRole is an administrator
var AdministratorRole = "administrator"

//UserRole ..
var UserRole = "user"

// Role is a user role
type Role struct {
	Name        string
	Description string
	Permissions []Permission
}

// TODO: Under construction
// CheckPermission returns true if the user is a member of a role that has the permission.
func (a *aclService) CheckPermission(user *domain.UserDetails, permission Permission) error {
	if user == nil {
		return errors.New("No user supplied")
	}
	if permission == "" {
		return errors.New("Invalid permission provided")
	}
	if user.HasRole(AdministratorRole) {
		return nil
	}
	if user.HasRole(string(permission)) {
		return nil
	}
	return errors.New("User not authorized")
}
