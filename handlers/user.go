package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gokars/leandervote/domain"
	"github.com/gokars/leandervote/services"
)

// Users provide API for user service
// Consider usage of Tokens here
type Users struct {
	Service services.Users
	Token   services.TokenService
}

// NewUsers creates a users handler
func NewUsers(s services.Users, t services.TokenService) *Users {
	//return &Users{s, store.User{}}
	return &Users{s, t}
}

// Login ...
func (u *Users) Login(c *gin.Context) {
	var user domain.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	authorized, admin, err := u.Service.AuthorizeWithRole(user.Email, user.Pass, "administrator")
	if !authorized || err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	// token expiration time in hours
	duration := 1
	claims := u.Token.GetAuthClaims(admin, user.Email, duration)
	token, err := u.Token.GetAuthToken(claims)
	if err != nil {
		// FIXME: What status here
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": token})
}

// Logout ...
func (u *Users) Logout(c *gin.Context) {
}
