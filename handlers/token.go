package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/gokars/leandervote/services"
)

// Tokens exposes an API to the tokens service
type Tokens struct {
	Service services.TokenService
}

// NewTokens creates new handler for tokens
func NewTokens(s services.TokenService) *Tokens {
	return &Tokens{s}
}

// New will return tokens
func (t *Tokens) New(c *gin.Context) {
}
