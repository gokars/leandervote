package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/gokars/leandervote/domain"
	log "github.com/sirupsen/logrus"
)

type authClaims struct {
	User  string
	Admin bool
}

// getAuthClaims fetches claims from context
func getAuthClaims(c *gin.Context) (authClaims, error) {
	var claims authClaims
	val, exists := c.Get("user")
	if !exists {
		return authClaims{}, domain.ErrInvalidClaims
	}
	claims.User = val.(string)

	val, exists = c.Get("admin")
	if !exists {
		return authClaims{}, domain.ErrInvalidClaims
	}
	claims.Admin = val.(bool)
	log.Debug("Admin rights for user '%s': %t", claims.User, claims.Admin)
	return claims, nil
}
