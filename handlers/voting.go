package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gokars/leandervote/domain"
	"github.com/gokars/leandervote/services"
	"github.com/gokars/leandervote/utils"
	log "github.com/sirupsen/logrus"
)

// Votings provide API for user service
type Votings struct {
	Service services.Votings
	Users   services.Users
	Tokens  services.TokenService
}

// NewVotings creates a users handler
func NewVotings(s services.Votings, u services.Users, t services.TokenService) *Votings {
	return &Votings{s, u, t}
}

// All ..
func (v *Votings) All(c *gin.Context) {
	results, err := v.Service.Get([]string{}, true)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	c.JSON(http.StatusOK, results)
}

// GetVotings ..
func (v *Votings) GetVotings(c *gin.Context) {
	claims, err := getAuthClaims(c)
	if err == domain.ErrInvalidClaims {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	var ids []string
	if !claims.Admin {
		ids = v.Users.GetVotings(claims.User)
		if len(ids) == 0 {
			c.Status(http.StatusOK)
			return
		}
	}
	results, err := v.Service.Get(ids, claims.Admin)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, results)
}

// Single ..
func (v *Votings) Single(c *gin.Context) {
	// FIXME: Validate id
	id := c.Param("id")
	result, err := v.Service.Single(id)
	if err != nil {
		// FIXME: What stauts if server has problem with retriving data
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, result)
}

// Action ..
func (v *Votings) Action(c *gin.Context) {
	// FIXME: Validate id
	id := c.Param("id")
	claims, err := getAuthClaims(c)
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	log.Debug("Action requested: %s", c.Param("action"))
	switch c.Param("action") {
	case "/invite":
		voting, err := v.Service.Single(id)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		// Get a map of voters
		voters := voting.Voters
		// token expiration period in hours
		duration := 24
		for index, voter := range voters {
			claims := v.Service.NewClaims(id, voter.Email, duration)
			tokenString, err := v.Tokens.GetVotingToken(claims)
			if err != nil {
				c.AbortWithStatus(http.StatusInternalServerError)
				return
			}
			_, hashString, err := utils.HashToken(tokenString)
			if err != nil {
				c.AbortWithStatus(http.StatusInternalServerError)
				return
			}
			voters[index].TokenHash = hashString
			log.Debug("Registering token '%s' generated for user '%s'", hashString, voter.Email)
		}
		v.Service.Invite(id, voters)
		c.Status(http.StatusOK)
	case "/voter":
		if claims.Admin {
			c.Status(http.StatusOK)
			return
		}
		info, err := v.Service.GetVoterStatus(id, claims.User)
		if err != nil {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		c.JSON(http.StatusOK, info)
	}
}

// Add ..
func (v *Votings) Add(c *gin.Context) {
	// TODO: Additional validation of Voting
	var form domain.Voting
	if err := c.ShouldBindJSON(&form); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	v.Service.Add(form)
	c.Status(http.StatusOK)
}

// Register ..
func (v *Votings) Register(c *gin.Context) {
	var form domain.RegisterForm
	if err := c.ShouldBindJSON(&form); err != nil {
		c.Status(http.StatusBadRequest)
		return
	}
	if form.Token == "" {
		c.Status(http.StatusBadRequest)
		return
	}
	if form.Pass == "" {
		c.Status(http.StatusBadRequest)
		return
	}
	token, claims, err := v.Tokens.ParseRegisterToken(form.Token)
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	if !token.Valid {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	log.Info("User '%s' provides valid token for voting '%d'", claims.Email, claims.Voting)

	used, tokenHash, err := v.Service.IsTokenUsed(claims.Voting, claims.Email)
	if used {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	_, currentTokenHash, _ := utils.HashToken(form.Token)
	if tokenHash != currentTokenHash {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// create user if not exists and append voting id to user's profile
	exists, err := v.Users.IsExists(claims.Email)
	if err != nil {
		// FIXME: What status here
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	if exists {
		ok, err := v.Users.Authorize(claims.Email, form.Pass)
		if err != nil {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		if !ok {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		_ = v.Users.AddVoting(claims.Email, claims.Voting)
	} else {
		err = v.Users.Create(claims.Email, form.Pass, claims.Voting)
		if err != nil {
			// FIXME: What status here
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
	}
	// Mark user as registered
	err = v.Service.SetUserRegistered(claims.Voting, claims.Email)
	if err != nil {
		// FIXME: What status here
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.Status(http.StatusOK)
}
