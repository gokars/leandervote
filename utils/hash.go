package utils

import (
	"crypto/sha256"
	"encoding/hex"

	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

// HashPass hashes a provided password
func HashPass(pass string) ([]byte, error) {
	hpass, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	return hpass, nil
}

// HashToken hashes provided token
func HashToken(token string) ([]byte, string, error) {
	h := sha256.New()
	_, err := h.Write([]byte(token))
	if err != nil {
		return nil, "", err
	}
	log.Debug("Generated hash: %x\n", h.Sum(nil))
	hash := h.Sum(nil)
	return hash, hex.EncodeToString(hash[:]), nil
}

// CompareHash compares hashed and plain string
func CompareHash(hash []byte, pass string) (bool, error) {
	err := bcrypt.CompareHashAndPassword(hash, []byte(pass))
	if err != nil {
		return false, err
	}
	return true, nil
}
