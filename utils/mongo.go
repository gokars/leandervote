package utils

import (
	"time"

	log "github.com/sirupsen/logrus"
	mgo "gopkg.in/mgo.v2"
)

// FIXME: Consider to move this to `common` package
var session *mgo.Session

// Config temp ...
type Config struct {
	Database    string
	MongoDBHost string
	DBUser      string
	DBPwd       string
}

// AppConfig temp ...
var AppConfig = Config{
	Database:    "leander",
	MongoDBHost: "localhost",
	DBUser:      "",
	DBPwd:       "",
}

func getSession() *mgo.Session {
	if session == nil {
		var err error
		session, err = mgo.DialWithInfo(&mgo.DialInfo{
			Addrs:    []string{AppConfig.MongoDBHost},
			Username: AppConfig.DBUser,
			Password: AppConfig.DBPwd,
			Timeout:  60 * time.Second,
		})
		if err != nil {
			log.Error("[utils] Get MongoDB session: %s\n", err)
		}
	}
	return session
}

func createDBSession() {
	var err error
	session, err = mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    []string{AppConfig.MongoDBHost},
		Username: AppConfig.DBUser,
		Password: AppConfig.DBPwd,
		Timeout:  60 * time.Second,
	})
	if err != nil {
		log.Error("[utils] Create MongoDB session: %s\n", err)
	}
}

// DataStore for MongoDB
type DataStore struct {
	MongoSession *mgo.Session
}

// Close mgo.Session
// Used to add defer statements for closing the copied sessions
func (ds *DataStore) Close() {
	ds.MongoSession.Close()
}

// Collection returns mgo.collection for the given name
func (ds *DataStore) Collection(name string) *mgo.Collection {
	return ds.MongoSession.DB(AppConfig.Database).C(name)
}

// NewDataStore creates a new DataAstore object to be used for each HTTP request
func NewDataStore() *DataStore {
	session := getSession().Copy()
	dataStore := &DataStore{
		MongoSession: session,
	}
	return dataStore
}
