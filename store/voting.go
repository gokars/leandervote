package store

import (
	"fmt"

	"github.com/gokars/leandervote/domain"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Voting ..
type Voting struct {
	C *mgo.Collection
}

// NewVotingStore ..
func NewVotingStore(col *mgo.Collection) *Voting {
	return &Voting{col}
}

// Get returns short info about Voting for provided ids
func (store Voting) Get(ids []string, admin bool) ([]domain.VotingShort, error) {
	var err error
	var results []domain.VotingShort
	if !admin {
		oids := make([]bson.ObjectId, len(ids))
		for i := range ids {
			oids[i] = bson.ObjectIdHex(ids[i])
		}
		query := bson.M{"_id": bson.M{"$in": oids}}
		err = store.C.Find(query).All(&results)
	} else {
		err = store.C.Find(nil).All(&results)
	}
	if err != nil {
		return nil, err
	}
	return results, nil
}

// Single returns specified Voting
func (store Voting) Single(id string) (domain.Voting, error) {
	var results domain.Voting
	err := store.C.FindId(bson.ObjectIdHex(id)).One(&results)
	if err != nil {
		return domain.Voting{}, err
	}
	return results, nil
}

// Create inserts new Voting to database
func (store Voting) Create(voting domain.Voting) error {
	err := store.C.Insert(voting)
	if err != nil {
		return err
	}
	return nil
}

// UpdateVoters updates batch of Voters for specified Voting
func (store Voting) UpdateVoters(id string, voters []domain.Voter) error {
	fmt.Println(voters)
	err := store.C.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"voters": voters}})
	if err != nil {
		return err
	}
	return nil
}

// UpdateVoter updates Voter info for specified Voting
func (store Voting) UpdateVoter(id string, voter domain.Voter) error {
	query := bson.M{
		"_id":          bson.ObjectIdHex(id),
		"voters.email": voter.Email,
	}
	update := bson.M{
		"$set": bson.M{
			"voters.$": voter,
		},
	}
	err := store.C.Update(query, update)
	if err != nil {
		return err
	}

	return nil
}

// GetVoter returns a Voter info for specified Voting
func (store Voting) GetVoter(id, email string) (domain.Voter, error) {
	// FIXME: better solution?
	type VotersContainer struct {
		Voters []domain.Voter
	}
	var result VotersContainer
	err := store.C.Find(bson.M{"_id": bson.ObjectIdHex(id)}).Select(bson.M{"voters": bson.M{"$elemMatch": bson.M{"email": email}}}).One(&result)
	if err != nil {
		return domain.Voter{}, err
	}
	return result.Voters[0], nil
}
