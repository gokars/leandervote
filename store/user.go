package store

import (
	"github.com/gokars/leandervote/domain"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// User provides persistence logic for "users" collection
type User struct {
	C *mgo.Collection
}

// NewUserStore ...
func NewUserStore(col *mgo.Collection) *User {
	return &User{col}
}

// Create insert new User
func (store User) Create(user domain.UserDetails) error {
	err := store.C.Insert(user)
	if err != nil {
		return err
	}
	return nil
}

// AddVoting adds a voting id to user's account
func (store User) AddVoting(email, voting string) error {
	push := bson.M{
		"$push": bson.M{
			"votings": voting,
		},
	}
	err := store.C.Update(bson.M{"email": email}, push)
	if err != nil {
		return err
	}

	return nil
}
