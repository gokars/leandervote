package domain

import (
	"gopkg.in/mgo.v2/bson"
)

// RegisterForm ..
type RegisterForm struct {
	Token string `json:"token"`
	Pass  string `json:"pass"`
}

// UserDetails ...
type UserDetails struct {
	ID      bson.ObjectId `bson:"_id" json:"id"`
	Email   string        `json:"email"`
	Hash    []byte        `json:"hash"`
	Name    string        `json:"name"`
	Roles   []string      `json:"roles"`
	Votings []string      `json:"votings"`
}

// User ...
type User struct {
	Email string `json:"email"`
	Name  string `json:"name"`
	Pass  string `json:"pass"`
}

// UserStore ...
type UserStore interface {
	Create(u UserDetails) (UserDetails, error)
	Remove(email string) error
	Update(email string) (UserDetails, error)
	Get(email string) (UserDetails, error)
	AddVoting(email string, voting string) error
}

// HasRole returns true if the user is in the role
func (u *UserDetails) HasRole(roleName string) bool {
	for _, role := range u.Roles {
		if role == roleName {
			return true
		}
	}
	return false
}
