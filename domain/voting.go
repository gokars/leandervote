package domain

import (
	"gopkg.in/mgo.v2/bson"
)

type (
	// Voting represents details about issued voting
	Voting struct {
		ID              bson.ObjectId        `bson:"_id,omitempty" json:"id,omitempty"`
		Name            string               `json:"name"`
		StartVotingDate string               `json:"startVotingDate"`
		StartVotingTime string               `json:"startVotingTime"`
		StartVotingInt  int64                `json:"-"`
		EndVotingDate   string               `json:"endVotingDate"`
		EndVotingTime   string               `json:"endVotingTime"`
		EndVotingInt    int64                `json:"-"`
		PreparationDays int                  `json:"preparationDays"`
		StartAuth       int64                `json:"startAuth"`
		StopAuth        int64                `json:"stopAuth"`
		PaperWallet     bool                 `json:"paperWallet"`
		VotingType      int                  `json:"votingType"` // 0 - individual, 1 - party
		Parties         []Party              `json:"parties"`
		Candidates      []Candidate          `json:"candidates"`
		VoterAccess     int                  `json:"voterAccess"` // 0 - email, 1 - domain
		Voters          []Voter              `json:"voters"`
		SmartContract   SmartContractDetails `json:"smartContract,omitempty"`
	}

	// VotingShort ..
	VotingShort struct {
		ID              bson.ObjectId `json:"id" bson:"_id"`
		Name            string        `json:"name"`
		StartVotingDate string        `json:"startVotingDate"`
		StartVotingTime string        `json:"startVotingTime"`
		EndVotingDate   string        `json:"endVotingDate"`
		EndVotingTime   string        `json:"endVotingTime"`
		PreparationDays int           `json:"preparationDays"`
		PaperWallet     bool          `json:"paperWallet"`
	}

	// Candidate represents person on which users can vote
	Candidate struct {
		ID        int    `json:"id"`
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
		PartyID   string `json:"party"`
	}

	// Party represents organization which consociate candidates
	Party struct {
		ID      int    `json:"id"`
		Name    string `json:"name"`
		Details string `json:"details"`
		// consider Location or smth
	}

	// Voter represent user allowed to vote by email
	Voter struct {
		UserID     string `json:"user"`
		Email      string `json:"email"`
		Registered bool   `json:"registered"`
		Authorized bool   `json:"authorized"`
		Signed     bool   `json:"signed"`
		TokenSent  bool   `json:"tokenSent"`
		TokenHash  string `json:"tokenHash"`
	}

	// VoterInfo ..
	VoterInfo struct {
		Registered bool `json:"registered"`
		Authorized bool `json:"authorized"`
		Signed     bool `json:"signed"`
	}

	// SmartContractDetails provide details to create contract code
	SmartContractDetails struct {
		Refund     bool   `json:"refund"`
		ChangeVote int    `json:"changeVote"`
		Token      bool   `json:"token"`
		TokenType  string `json:"tokenType"`
	}

	// VotingStore ..
	VotingStore interface {
		Get([]string, bool) ([]VotingShort, error)
		Add(Voting) error
		Single(string) (Voting, error)
		UpdateVoters(string, map[string]Voter) error
		GetVoter(id, email string) (Voter, error)
		UpdateVoter(id string, voter Voter) error
	}
)
