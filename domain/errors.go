package domain

import "errors"

// TODO: Custom struct for errors and compare mechanism
var (
	// ErrInvalidAlogrithm is thrown when algorithm is not recognized
	ErrInvalidAlogrithm = errors.New("Invalid algorithm type")
	// ErrInvalidClaims is thrown when problem with parsing claims occurs
	ErrInvalidClaims = errors.New("Invalid claims")
	// ErrHashingError is thrown when hashing function returns some error
	ErrHashPass = errors.New("Problem with hashing")
	// ErrCreateDatabaseRecord is thrown when problem occurs with creating record in DB
	ErrCreateDatabaseRecord = errors.New("Problem with creating record in DB")
	// ErrUserNotFound is thrown if user doesn't exists in DB
	ErrUserNotFound = errors.New("User not found in DB")
	// ErrUserNotFound is thrown if DB engine returns error during executing query
	ErrQueryDatabase = errors.New("Problem with DB query")
)
